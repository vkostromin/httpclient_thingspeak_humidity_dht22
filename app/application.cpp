#include <user_config.h>
#include <SmingCore/SmingCore.h>
#include <Libraries/DHT/DHT.h>

// If you want, you can define WiFi settings globally in Eclipse Environment Variables
//or define WiFi settings in user_config.h
#ifndef WIFI_SSID
	#define WIFI_SSID "WIFI_SSID" // Put you SSID and Password here
	#define WIFI_PWD "WIFI_PWD"
#endif

#ifndef API_KEY
	#define API_KEY "API_KEY" // Put you SSID and Password here
#endif

#define WORK_PIN 14 // GPIO14

DHT dht(WORK_PIN, DHT22);

Timer procTimer;
HttpClient thingSpeak;

void onDataSent(HttpClient& client, bool successful)
{
	if (successful)
		Serial.println("Success sent");
	else
		Serial.println("Failed");

	String response = client.getResponseString();
	Serial.println("Server response: '" + response + "'");
	if (response.length() > 0)
	{
		int intVal = response.toInt();

		if (intVal == 0)
			Serial.println("Sensor value wasn't accepted. May be we need to wait a little?");
	}
}

void sendData()
{
	if (thingSpeak.isProcessing()) return; // We need to wait while request processing was completed

	/* improved reading method */
	Serial.print("\nRead using new API methods\n");
	TempAndHumidity th;
	if(dht.readTempAndHumidity(th))
	{
		Serial.print("\tHumidity: ");
		Serial.print(th.humid);
		Serial.print("% Temperature: ");
		Serial.print(th.temp);
		Serial.print(" *C\n");
	}
	else
	{
		Serial.print("Failed to read from DHT: ");
		Serial.print(dht.getLastError());
	}

	//thingSpeak.downloadString("https://api.thingspeak.com/update?api_key=" + String(API_KEY) + "&field1=" + String(sensorValue), onDataSent);
	thingSpeak.downloadString("http://api.thingspeak.com/update?key=" + String(API_KEY) + "&field1=" + String(th.humid) + "&field2=" + String(th.temp), onDataSent);

}

// Will be called when WiFi station was connected to AP
void connectOk()
{
	Serial.println("I'm CONNECTED");
	
	dht.begin();

	// Start send data loop
	procTimer.initializeMs(5 * 60 * 1000, sendData).start(); // every 5 minutes
}

// Will be called when WiFi station timeout was reached
void connectFail()
{
	Serial.println("I'm NOT CONNECTED. Need help :(");

	// Start soft access point
	WifiAccessPoint.enable(true);
	WifiAccessPoint.config("CONFIG ME PLEEEEASE...", "", AUTH_OPEN);

	// .. some you code for configuration ..
}

void init()
{
	spiffs_mount(); // Mount file system, in order to work with files

	Serial.begin(SERIAL_BAUD_RATE); // 115200 by default
	Serial.systemDebugOutput(false); // Disable debug output to serial

	WifiStation.config(WIFI_SSID, WIFI_PWD);
	WifiStation.enable(true);
	WifiAccessPoint.enable(false);

	// Run our method when station was connected to AP (or not connected)
	WifiStation.waitConnection(connectOk, 20, connectFail); // We recommend 20+ seconds for connection timeout at start
}
